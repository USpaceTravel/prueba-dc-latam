document.addEventListener("DOMContentLoaded", function() {
    const searchButton = document.getElementById("searchButton");
    const employeeIdInput = document.getElementById("employeeId");
    const contentDiv = document.getElementById("content");
    const listEmployeesLink = document.getElementById("listEmployees");

    searchButton.addEventListener("click", function() {
        const employeeId = employeeIdInput.value.trim();
        
        if (employeeId === "") {
            fetchEmployees();
        } else {
            fetchEmployeeDetails(employeeId);
        }
    });

    listEmployeesLink.addEventListener("click", function() {
        fetchEmployees();
    });

    function fetchEmployees() {
        fetch("http://localhost:8080/employees")
            .then(response => response.json())
            .then(data => displayEmployeeList(data))
            .catch(error => {
                contentDiv.innerHTML = `<p>Error del servidor</p>`;
                console.error("Error fetching employees:", error)});
    }

    async function fetchEmployeeDetails(employeeId) {

    try{

        const response = await fetch(`http://localhost:8080/employee/${employeeId}`).catch(err => contentDiv.innerHTML = `<p>Error del servidor</p>`);
                const employee = await response.json();


                if(response.status == 429){

                    contentDiv.innerHTML = `<p>Error del servidor 429, Muchas peticiones</p>`;
                }
                else if(response.status == 200 && employee != null){

                    displayEmployeeDetails(employee);

                }
                else if(response.status == 400 ){

                    contentDiv.innerHTML = `<p>Usuario no encontrado</p>`;
                }
                else{

                    console.error("Error fetching employee details:");
                }

    }
    catch(e){
        contentDiv.innerHTML = `<p>Error del servidor</p>`;
    }

    }

    function displayEmployeeList(employees) {
        let content = "<h3>Lista de Empleados</h3>";
        content += "<table class='table'><thead><tr><th>ID</th><th>Nombre</th><th>Salario</th><th>Salario Anual</th></tr></thead><tbody>";

        employees.forEach(employee => {
            content += `<tr><td>${employee.id}</td><td>${employee.employee_name}</td><td>${employee.employee_salary}</td><td>${employee.employee_salary * 12}</td></tr>`;
        });

        content += "</tbody></table>";
        contentDiv.innerHTML = content;
    }

    function displayEmployeeDetails(employee) {

        if(employee != null){

            let content = "<h3>Detalles del Empleado</h3>";
            let annualSalary = employee.employee_salary * 12;
            content += `<p><strong>ID:</strong> ${employee.id}</p>`;
            content += `<p><strong>Nombre:</strong> ${employee.employee_name}</p>`;
            content += `<p><strong>Salario:</strong> ${employee.employee_salary}</p>`;
            content += `<p><strong>Edad:</edad> ${employee.employee_age}</p>`;
            content += `<p><strong>Salario Anual:</strong> ${annualSalary}</p>`;

            contentDiv.innerHTML = content;
        }
        else{
            contentDiv.innerHTML = `<p>User no found</p>`;
        }

    }
});
