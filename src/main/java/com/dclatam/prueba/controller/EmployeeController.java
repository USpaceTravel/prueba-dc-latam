package com.dclatam.prueba.controller;

import com.dclatam.prueba.model.Employee;
import com.dclatam.prueba.model.ResponseModel;
import com.dclatam.prueba.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.io.IOException;
import java.util.List;


@RestController
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping(value = "/employees", produces = "application/json")
    @ResponseBody
    public ResponseEntity<List<Employee>> getAllEmployees() {

        try {

            ResponseModel res = employeeService.getAllEmployees();
            List<Employee> employee = res.getData();

            return new ResponseEntity<>(employee, res.getResponse());


        }
        catch (Exception e) {

            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/employee/{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable int id) {

        try {
            ResponseModel res = employeeService.getEmployeeById(id);

            return new ResponseEntity<>(res.getData().get(0), res.getResponse());

        }
        catch (Exception e){

            System.out.println(e);
            return new ResponseEntity<>( HttpStatus.BAD_REQUEST);
        }

    }
}
