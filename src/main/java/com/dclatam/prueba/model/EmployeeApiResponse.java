package com.dclatam.prueba.model;

import com.fasterxml.jackson.annotation.JsonProperty;


public class EmployeeApiResponse {
    @JsonProperty("status")
    private String status;
    @JsonProperty("data")
    private Employee data;
    @JsonProperty("message")
    private String message;

    @Override
    public String toString() {
        return "EmployeeApiResponse{" +
                "status='" + status + '\'' +
                ", data=" + data +
                ", message='" + message + '\'' +
                '}';
    }

    public Employee getData() {
        return data;
    }

    public void setData(Employee data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
