package com.dclatam.prueba.model;

import org.springframework.http.HttpStatus;

import java.util.List;

public class ResponseModel {

    HttpStatus response;

    List<Employee> data;

    public HttpStatus getResponse() {
        return response;
    }

    public void setResponse(HttpStatus response) {
        this.response = response;
    }

    public List<Employee> getData() {
        return data;
    }

    public void setData(List<Employee> data) {
        this.data = data;
    }
}
