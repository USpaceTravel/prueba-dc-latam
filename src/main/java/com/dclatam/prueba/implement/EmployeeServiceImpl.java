package com.dclatam.prueba.implement;

import com.dclatam.prueba.model.EmployeeApiResponse;
import com.dclatam.prueba.model.EmployeesApiResponse;
import com.dclatam.prueba.model.ResponseModel;
import com.dclatam.prueba.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.dclatam.prueba.model.Employee;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    private static final String EMPLOYEE_API_URL = "http://dummy.restapiexample.com/api/v1/";

    @Autowired
    private RestTemplate restTemplate;

    public EmployeeServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public ResponseModel getAllEmployees() {

        String url = EMPLOYEE_API_URL + "employees/";
        ResponseModel res = new ResponseModel();

        try {
            String finalUrl = followRedirects(url);
            EmployeesApiResponse response = restTemplate.getForObject(finalUrl, EmployeesApiResponse.class);
            if (response.getData() != null){

                res.setResponse(HttpStatus.OK);
                res.setData(response.getData());
                return res;
            }
            else {
                res.setResponse(HttpStatus.BAD_REQUEST);
                res.setData(null);
                return res;

            }

        }
        catch (Exception e){
            System.out.println(e+ " Error");
            return null;
        }

    }

    @Override
    public ResponseModel getEmployeeById(int id) {

        try{

            String url = EMPLOYEE_API_URL + "employee/" + id;
            String finalUrl = followRedirects(url);
            EmployeeApiResponse response = restTemplate.getForObject(finalUrl, EmployeeApiResponse.class);
            ResponseModel res = new ResponseModel();


            if (response.getData() != null ) {

                res.setResponse(HttpStatus.OK);
                Employee a = response.getData();
                List<Employee> list = new ArrayList<>();
                list.add(a);

                res.setData(list);
                return res;
            }
            else if(response.getData() == null) {

                res.setResponse(HttpStatus.NOT_FOUND);
                res.setData(null);
                return res;
            }
            else {
                res.setResponse(HttpStatus.BAD_REQUEST);
                res.setData(null);
                return res;
            }
        }

        catch (Exception e){
            System.out.println(e.getCause());
            return null;
        }

    }
    //Manejo de redirecciones.
    private String followRedirects(String initialUrl) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) new URL(initialUrl).openConnection();
        connection.setRequestMethod("HEAD"); // Solicitud HEAD para obtener encabezados
        connection.setInstanceFollowRedirects(false);
        int responseCode = connection.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_MOVED_PERM || responseCode == HttpURLConnection.HTTP_MOVED_TEMP) {
            return connection.getHeaderField("Location"); // Obtener la nueva URL de la cabecera de redirección
        }
        return null;
    }
}
