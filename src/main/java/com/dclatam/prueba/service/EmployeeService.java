package com.dclatam.prueba.service;

import com.dclatam.prueba.model.Employee;
import com.dclatam.prueba.model.ResponseModel;

import java.io.IOException;
import java.util.List;

public interface EmployeeService {
    ResponseModel getAllEmployees();
    ResponseModel getEmployeeById(int id);
}
