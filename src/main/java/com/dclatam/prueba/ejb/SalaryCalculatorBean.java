package com.dclatam.prueba.ejb;

import org.springframework.http.MediaType;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

@Path("/salary")
@Stateless
public class SalaryCalculatorBean {

    @GET
    @Path("/calculate")
    @Produces()
    public double calculateAnnualSalary(@QueryParam("salary") double salary) {
        return salary * 12;
    }
}