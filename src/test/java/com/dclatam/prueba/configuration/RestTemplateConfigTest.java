package com.dclatam.prueba.configuration;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RestTemplateConfigTest {

    private RestTemplateConfig restTemplateConfig;

    @BeforeEach
    public void setup() {
        restTemplateConfig = new RestTemplateConfig();
    }

    @Test
    public void testRestTemplateCreation() {
        RestTemplate restTemplate = restTemplateConfig.restTemplate();
        List<HttpMessageConverter<?>> converters = restTemplate.getMessageConverters();

        assertEquals(MappingJackson2HttpMessageConverter.class, converters.get(0).getClass());

    }
}