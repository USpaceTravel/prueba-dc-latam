package com.dclatam.prueba.ejb;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import javax.ws.rs.core.Response;

import static org.mockito.Mockito.when;

public class SalaryCalculatorBeanTest {

    @InjectMocks
    private SalaryCalculatorBean salaryCalculatorBean;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCalculateAnnualSalary() {
        double salary = 50000;
        double expectedAnnualSalary = salary * 12;

        double result = salaryCalculatorBean.calculateAnnualSalary(salary);

        assertEquals(expectedAnnualSalary, result, 0.01);
    }
}
