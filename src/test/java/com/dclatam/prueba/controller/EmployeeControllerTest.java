package com.dclatam.prueba.controller;

import com.dclatam.prueba.model.Employee;
import com.dclatam.prueba.model.ResponseModel;
import com.dclatam.prueba.service.EmployeeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class EmployeeControllerTest {
    @Mock
    private EmployeeService employeeService;

    private EmployeeController employeeController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        employeeController = new EmployeeController(employeeService);
    }

    @Test
    public void testGetAllEmployees() {
        // Arrange
        List<Employee> mockEmployees = new ArrayList<>();
        Employee emp = new Employee();
        emp.setEmployeeName("Jorge");
        emp.setEmployeeAge(25);
        emp.setId(1);
        emp.setEmployeeSalary(22000);
        emp.setProfileImage("");

        mockEmployees.add(emp);
        ResponseModel responseModel = new ResponseModel();
        responseModel.setResponse(HttpStatus.OK);
        responseModel.setData(mockEmployees);

        when(employeeService.getAllEmployees()).thenReturn(responseModel);

        // Act
        ResponseEntity<List<Employee>> responseEntity = employeeController.getAllEmployees();

        // Assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(mockEmployees, responseEntity.getBody());
    }

    @Test
    public void testGetEmployeeById() {
        // Arrange
        int employeeId = 1;
        Employee emp = new Employee();
        emp.setEmployeeName("Jorge");
        emp.setEmployeeAge(25);
        emp.setId(1);
        emp.setEmployeeSalary(22000);
        emp.setProfileImage("");
        List<Employee> list = new ArrayList<>();
        list.add(emp);
        ResponseModel responseModel = new ResponseModel();
        responseModel.setResponse(HttpStatus.OK);
        responseModel.setData(list);

        when(employeeService.getEmployeeById(employeeId)).thenReturn(responseModel);

        // Act
        ResponseEntity<Employee> responseEntity = employeeController.getEmployeeById(employeeId);

        // Assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(list.get(0), responseEntity.getBody());
    }
}
